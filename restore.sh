#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$DIR/config.cfg"
echo "$(date) ...starting restore" | tee  restore.log
databases=`ssh $remote_server "ls ${bak_dir}/"`


if [ -z $1]
then
  echo "missing parameter"
  echo "--latest   -   for latest backup"
  echo "or take one from this list:"
  echo ""
  echo $databases;
  echo ""
else
  if [ $1 == "--latest" ]
  then
    echo "$(date) ...get latest backup" | tee -a restore.log
    scp $remote_server:`ssh ${remote_server} ls -1td ${bak_dir}/\* | head -1` "${DIR}/${db_name}.zip"
  else
    for db in $databases
    do
      if [ $1 == $db ]
      then
        echo "$(date) ...get selected backup" | tee -a restore.log
        scp $remote_server:"${bak_dir}/${db}" "${DIR}/${db_name}.zip"        
      fi
    done
  fi
fi


if [ -f "${DIR}/${db_name}.zip" ]
then
    echo "$(date) ...backupfile zip exists" | tee -a restore.log
    echo "$(date) ...unzip zip backup file" | tee -a restore.log
    unzip -o -p "${DIR}/${db_name}.zip" > "${DIR}/${db_name}.bak"
    echo "$(date) ...remove zip backup file" | tee -a restore.log
    rm "${DIR}/${db_name}.zip"
fi


if [ -f "${db_name}.bak" ]
then
    echo "$(date) ...backupfile bak exists" | tee -a restore.log

    for t in $docker_targets
    do
      echo "$(date) ...copy bak file to target ${t}" | tee -a restore.log
      docker cp "${DIR}/${db_name}.bak" $t:/var/opt/mssql/data
    done
    for s in $server_targets
    do
      echo "$(date) ...restore mssql to server ${s}" | tee -a restore.log
      docker run -it --network=host --rm salamander1/sqlcmd -S $s -U$db_user -P$db_pass -Q "RESTORE DATABASE ${db_name} FROM DISK='/var/opt/mssql/data/${db_name}.bak' WITH MOVE '${db_name}' TO '/var/opt/mssql/data/${db_name}.MDF', MOVE '${db_name}_log' TO '/var/opt/mssql/data/${db_name}_log.ldf'" 
    done
    rm "${DIR}/${db_name}.bak"
fi











